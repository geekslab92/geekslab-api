<?php
    namespace App\Controller;

    use App\Controller\AppController;


    class UsersController extends AppController
    {

        public function initialize()
        {
            parent::initialize();
            $this->loadComponent('User');
        }

        public function index()
        {
            return $this->redirect(['action'=>'login']);
        }

        public function login()
        {

            if ($this->request->is('post')) {
                $user = $this->Auth->identify();

                if ($user) {
                    $roleid = $user['role_id'];

                    if($roleid == 1){
                        $this->Auth->setUser($user);
                        return $this->redirect(array('prefix' => 'admin', 'controller' => 'Dashboard', 'action' => 'index'));
                    }else{
                        $this->Flash->error('Connection non authorizer');
                    }

                }else{
                    $this->Flash->error('Your username or password is incorrect.');
                }




            }

        }

        public function dispatcher()
        {
            if ($this->Auth->user('id') == null) {
                return $this->redirect(array('action' => 'login'));
            }else{

                $roleid = $this->Auth->user('role_id');
                $role = $this->Users->Roles->get($roleid, array('fields' => ['alias']));
                $prefix = $role->alias;
                return $this->redirect(array('prefix' => $prefix, 'controller' => 'Dashboard', 'action' => 'index'));

            }
        }

        public function logout()
        {
            $this->Flash->success('You are now logged out.');
            return $this->redirect($this->Auth->logout());
        }

        public function signup()
        {
            $user = $this->Users->newEntity();
            if ($this->request->is('post')) {
                $data=$this->request->getData();
                $user = $this->Users->patchEntity($user, $data);

                /* Assign default role and formatting the new user */
                $user->role_id=2;
                $user= $this->User->newUserFormatting($user);

                if ($this->Users->save($user)) {
                    $this->Flash->success(__('Sing Up Success.'));

                    return $this->redirect(['controller' => 'Dashboard','action' => 'index']);
                }
                $this->Flash->error(__('The user could not be saved. Please, try again.'));
            }
            $this->set(compact('user'));

        }

    }
