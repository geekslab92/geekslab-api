<?php
namespace App\Controller\Component;

use Cake\Controller\Component;
use Cake\Controller\ComponentRegistry;
use Cake\ORM\TableRegistry;

/**
 * Users component
 */
class UserComponent extends Component
{
    /**
     * Default configuration.
     *
     * @var array
     */
    protected $_defaultConfig = [];

    public function newUserFormatting($user=NULL){

        $this->Users = TableRegistry::get('Users');
        $this->Profiles = TableRegistry::get('Profiles');

        /* creating Active and token propriety */
        if(1==1){
            if($user->role_id == 1){
                $user->active=1;
                $user->token=NULL;
            }else{
                $user->active=0;
                $user->token=$this->Users->generateToken();
            }
        }

        /* creating Profiles */
        if(1==1){
            $profile=$this->Profiles->newEntity();
            $profile->civility_id=1;
            $profile->country_id=1;
            $savedprofile=$this->Profiles->save($profile);
            $user->profile_id=$savedprofile->id;
        }

        return $user;

    }
}
