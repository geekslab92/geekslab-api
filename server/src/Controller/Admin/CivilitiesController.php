<?php
namespace App\Controller\Admin;

use App\Controller\AppController;

class CivilitiesController extends AppController
{

    public function index()
    {
        $civilities = $this->paginate($this->Civilities);

        $this->set(compact('civilities'));
    }


    public function add()
    {
        $civility = $this->Civilities->newEntity();
        if ($this->request->is('post')) {
            $civility = $this->Civilities->patchEntity($civility, $this->request->getData());
            if ($this->Civilities->save($civility)) {
                $this->Flash->success(__('The civility has been saved.'));

                return $this->redirect(['action' => 'index']);
            }
            $this->Flash->error(__('The civility could not be saved. Please, try again.'));
        }
        $this->set(compact('civility'));
    }


    public function edit($id = null)
    {
        $civility = $this->Civilities->get($id, [
            'contain' => [],
        ]);
        if ($this->request->is(['patch', 'post', 'put'])) {
            $civility = $this->Civilities->patchEntity($civility, $this->request->getData());
            if ($this->Civilities->save($civility)) {
                $this->Flash->success(__('The civility has been saved.'));

                return $this->redirect(['action' => 'index']);
            }
            $this->Flash->error(__('The civility could not be saved. Please, try again.'));
        }
        $this->set(compact('civility'));
    }

    public function delete($id = null)
    {
        $this->request->allowMethod(['post', 'delete']);
        $civility = $this->Civilities->get($id);
        if ($this->Civilities->delete($civility)) {
            $this->Flash->success(__('The civility has been deleted.'));
        } else {
            $this->Flash->error(__('The civility could not be deleted. Please, try again.'));
        }

        return $this->redirect(['action' => 'index']);
    }
}
