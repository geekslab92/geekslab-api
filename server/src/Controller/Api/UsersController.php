<?php
namespace App\Controller\Api;

use Cake\Core\Configure;
use Cake\Event\Event;
use Cake\Network\Exception\UnauthorizedException;
use Cake\Utility\Security;
use Firebase\JWT\JWT;

class UsersController extends AppController
{
    public function initialize()
    {
        parent::initialize();
        $this->loadComponent('User');
        $this->Auth->allow(['register', 'token', 'reset']);
    }

    public function register()
    {
        $user = $this->Users->newEntity();
        if ($this->request->is('post')) {


            $data=$this->request->getData();
            $user = $this->Users->patchEntity($user, $data);

            /* Assign default role and formatting the new user */
            $user->role_id=2;
            $user= $this->User->newUserFormatting($user);

            /* Save the user */
            $savedUser= $this->Users->save($user);

            if(!empty($savedUser)){
                $this->set([
                    'success' => true,
                    'data' => [
                        'myid' => $savedUser->id,
                        'myEmail' => $savedUser->email,
                        'token' => JWT::encode([
                            'sub' => $savedUser->id,
                            'iat' => time(),
                            'exp' =>  time() + 1200, // 20 min
                        ],
                        Security::salt())
                    ],
                    '_serialize' => ['success', 'data']
                ]);
            }else{
                throw new UnauthorizedException('The user could not be saved. Please, try again.');
            }
        }


    }

    public function token()
    {

        $this->layout=false;
        $autorizedUser=0;
        if ($this->request->is('post')) {
            $data=$this->request->getData();
            $email=$data['email'];
            $password=$data['password'];
            $user=$this->Users->findByEmail($email)->first();

            if (!$user) {
                throw new UnauthorizedException('Invalid Email');
            }else{
                $userpass=$user->password;
                $match = $this->Users->checkPassword($password, $userpass);
                if($match == true ){
                    if($user->active == 1){
                        $autorizedUser=1;
                    }else{
                        throw new UnauthorizedException('Account not activated');
                    }

                }else{
                    throw new UnauthorizedException('Invalid password');
                }
            }

        }


        if ($autorizedUser == 0) {
            throw new UnauthorizedException('Invalid username or password');
        }else{

            $this->set([
                'success' => true,
                'data' => [
                    'token' => JWT::encode([
                        'email'=>$user['email'],
                        'sk'=>md5($user['password'].Configure::read('ApiSecure')),
                        'iss'=>'Geekslab-api',
                        'sub' => $user['id'],
                        'exp' =>  time() + 604800
                    ],
                    Security::salt())
                ],
                '_serialize' => ['success', 'data']
            ]);
        }



    }

    public function reset()
    {

        if ($this->request->is('post')) {
            $data=$this->request->getData();
            $email=$data['email'];
            $user=$this->Users->findByEmail($email)->first();
            if(isset($user) and !empty($user)){
                $user->token=$this->Users->generateToken();
                if($this->Users->save($user)){
                    $msg="success";
                    $this->set([
                        'posts' => $msg,
                        '_serialize' => ['posts']
                    ]);
                }else{
                    throw new UnauthorizedException('Sended Mail.');
                }
            }else{
                throw new UnauthorizedException('User not found');
            }



        }else{
            throw new UnauthorizedException('You are not autorized to acceess this location');
        }

    }



}
