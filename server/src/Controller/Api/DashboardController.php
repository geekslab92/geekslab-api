<?php
namespace App\Controller\Api;

use App\Controller\Api\AppController;
use Cake\Network\Exception\UnauthorizedException;

/**
 * Dashboard Controller
 *
 *
 * @method \App\Model\Entity\Dashboard[]|\Cake\Datasource\ResultSetInterface paginate($object = null, array $settings = [])
 */
class DashboardController extends AppController
{
    public function initialize()
    {
        parent::initialize();
        $this->loadComponent('SecureJwt');
    }

    public function index()
    {
        /* second security lvl */
        $allow=0;
        $header=$this->request->getHeader('Authorization');
        $allow= $this->SecureJwt->checkKey($header);

        if($allow == 1){
            $msg='no think to see here';
            $this->set([
                'msg' => $msg,
                '_serialize' => ['msg']
            ]);

        }else{
            throw new UnauthorizedException('Non authorizer');
        }

    }
}
