import { TestBed } from '@angular/core/testing';

import { AfterLogginService } from './after-loggin.service';

describe('AfterLogginService', () => {
  beforeEach(() => TestBed.configureTestingModule({}));

  it('should be created', () => {
    const service: AfterLogginService = TestBed.get(AfterLogginService);
    expect(service).toBeTruthy();
  });
});
