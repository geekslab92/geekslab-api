import { TestBed } from '@angular/core/testing';

import { BeforeLogginService } from './before-loggin.service';

describe('BeforeLogginService', () => {
  beforeEach(() => TestBed.configureTestingModule({}));

  it('should be created', () => {
    const service: BeforeLogginService = TestBed.get(BeforeLogginService);
    expect(service).toBeTruthy();
  });
});
