import { TestBed } from '@angular/core/testing';

import { ExternalResquestService } from './external-resquest.service';

describe('ExternalResquestService', () => {
  beforeEach(() => TestBed.configureTestingModule({}));

  it('should be created', () => {
    const service: ExternalResquestService = TestBed.get(ExternalResquestService);
    expect(service).toBeTruthy();
  });
});
