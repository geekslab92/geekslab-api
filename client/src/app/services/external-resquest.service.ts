import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';

@Injectable({
  providedIn: 'root'
})
export class ExternalResquestService {

  private baseUrl = 'http://localhost:8765/api';

  constructor(private http: HttpClient) { }

  login(data) {
    return this.http.post(`${this.baseUrl}/users/token.json` ,data);
  }

  signup(data) {
    return this.http.post(`${this.baseUrl}/users/register.json`, data);
  }

  resetPassword(data){
    return this.http.post(`${this.baseUrl}/users/reset.json`, data);
  }

}
