import { Component, OnInit } from '@angular/core';
import { ExternalResquestService } from 'src/app/services/external-resquest.service';
import { TokenService } from 'src/app/services/token.service';
import { Router } from '@angular/router';
import { AuthService } from 'src/app/services/auth.service';

@Component({
  selector: 'app-login',
  templateUrl: './login.component.html',
  styleUrls: ['./login.component.scss']
})
export class LoginComponent implements OnInit {

  public form = {
    email: null,
    password: null
  };

  public error = null;

  constructor(
    private ExternalRequest: ExternalResquestService,
    private Token: TokenService,
    private Router: Router,
    private Auth: AuthService
    ) { }

  onSubmit() {
    this.ExternalRequest.login(this.form).subscribe(
      data => this.handleResponse(data),
      error => this.handleError(error)
    );
  }

  handleResponse(data) {
    this.Token.handle(data.data.token);
    this.Auth.changeAuthStatus(true);
    this.Router.navigateByUrl('/profile');
  }
  handleError(error) {
    this.error = error.error.message;
  }

  ngOnInit() {
  }

}
