import { Component, OnInit } from '@angular/core';
import { ExternalResquestService } from 'src/app/services/external-resquest.service';
import { TokenService } from 'src/app/services/token.service';
import { Router } from '@angular/router';


@Component({
  selector: 'app-signup',
  templateUrl: './signup.component.html',
  styleUrls: ['./signup.component.scss']
})
export class SignupComponent implements OnInit {

  public form = {
    email: null,
    password: null,
    confirm_password: null
  };

  public error = null;


  constructor(
    private ExternalRequest: ExternalResquestService,
    private Token: TokenService,
    private Router: Router
    ) { }

  onSubmit() {
    this.ExternalRequest.signup(this.form).subscribe(
      data => this.handleResponse(data),
      error => this.handleError(error)
    );
  }

  handleResponse(data) {
    this.Token.handle(data.data.token);
    this.Router.navigateByUrl('/profile');
  }
  handleError(error) {
    this.error = error.error.message;
  }

  ngOnInit() {
  }

}
