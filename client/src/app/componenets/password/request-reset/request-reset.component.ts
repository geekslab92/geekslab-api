import { Component, OnInit } from '@angular/core';
import { ExternalResquestService } from 'src/app/services/external-resquest.service';
import { Router } from '@angular/router';
import { SnotifyService } from 'ng-snotify';



@Component({
  selector: 'app-request-reset',
  templateUrl: './request-reset.component.html',
  styleUrls: ['./request-reset.component.scss']
})
export class RequestResetComponent implements OnInit {

  public form = {
    email : null
  };

  public error = null;

  constructor(
    private ExternalRequest: ExternalResquestService,
    private Router: Router,
    private snotifyService: SnotifyService
  ) { }

  ngOnInit() {
  }

  onSubmit() {
    this.ExternalRequest.resetPassword(this.form).subscribe(
      data => this.handleResponse(data),
      error => this.handleError(error)
    );
  }

  handleResponse(data) {
    this.form.email=null;
    this.snotifyService.confirm('Done!');
    this.Router.navigateByUrl('/login');
  }
  handleError(error) {
    this.error = error.error.message;
  }

}
