import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';
import { FormsModule } from '@angular/forms';

import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import { MaterialModule } from './material/material.module';
import { NavbarComponent } from './componenets/navbar/navbar.component';
import { LoginComponent } from './componenets/login/login.component';
import { SignupComponent } from './componenets/signup/signup.component';
import { ProfileComponent } from './componenets/profile/profile.component';
import { RequestResetComponent } from './componenets/password/request-reset/request-reset.component';
import { ResponseResetComponent } from './componenets/password/response-reset/response-reset.component';
import { ListPostComponent } from './componenets/posts/list-post/list-post.component';
import { HttpClientModule } from '@angular/common/http';
import { ExternalResquestService } from './services/external-resquest.service';
import { TokenService } from './services/token.service';
import { AuthService } from './services/auth.service';
import { BeforeLogginService } from './services/before-loggin.service';
import { AfterLogginService } from './services/after-loggin.service';
import { SnotifyModule, SnotifyService, ToastDefaults } from 'ng-snotify';





@NgModule({
  declarations: [
    AppComponent,
    NavbarComponent,
    LoginComponent,
    SignupComponent,
    ProfileComponent,
    RequestResetComponent,
    ResponseResetComponent,
    ListPostComponent
  ],
  imports: [
    BrowserModule,
    AppRoutingModule,
    FormsModule,
    HttpClientModule,
    BrowserAnimationsModule,
    MaterialModule,
    BrowserModule,
    SnotifyModule
  ],
  providers: [
    ExternalResquestService,
    TokenService,
    AuthService,
    BeforeLogginService,
    AfterLogginService,
    { provide: 'SnotifyToastConfig', useValue: ToastDefaults},
    SnotifyService
   ],
  bootstrap: [AppComponent]
})
export class AppModule { }
