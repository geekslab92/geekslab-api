import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { LoginComponent } from './componenets/login/login.component';
import { SignupComponent } from './componenets/signup/signup.component';
import { ProfileComponent } from './componenets/profile/profile.component';
import { RequestResetComponent } from './componenets/password/request-reset/request-reset.component';
import { ResponseResetComponent } from './componenets/password/response-reset/response-reset.component';
import { ListPostComponent } from './componenets/posts/list-post/list-post.component';
import { BeforeLogginService } from './services/before-loggin.service';
import { AfterLogginService } from './services/after-loggin.service';


const routes: Routes = [
  {
    path: 'login',
    component: LoginComponent,
    canActivate: [BeforeLogginService]
  },
  {
    path: 'signup',
    component: SignupComponent,
    canActivate: [BeforeLogginService]
  },
  {
    path: 'profile',
    component: ProfileComponent,
    canActivate: [AfterLogginService]
  },
  {
    path: 'reset-password',
    component: RequestResetComponent,
    canActivate: [BeforeLogginService]
  },
  {
    path: 'new-password',
    component: ResponseResetComponent,
    canActivate: [AfterLogginService]
  },
  {
    path: 'posts-list',
    component: ListPostComponent,
    canActivate: [AfterLogginService]
  },
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }
